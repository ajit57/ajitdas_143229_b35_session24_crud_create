<?php


namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class BookTitle extends DB
{
    public $id = "";
    public $book_title = "";
    public $author_name = "";

    public function __construct()
    {

        parent::__construct();
    }

    public function index()
    {
        echo "Book Title will go here";
    }

    public function setData($postVaribaleData = NULL)
    {
        if (array_key_exists('id', $postVaribaleData)) {
            $this->id = $postVaribaleData['id'];
        }
        if (array_key_exists('book_title', $postVaribaleData)) {
            $this->book_title = $postVaribaleData['book_title'];
        }
        if (array_key_exists('author_name', $postVaribaleData)) {
            $this->author_name = $postVaribaleData['author_name'];
        }
    }

    public function store()
    {
        $arrData = array($this->book_title, $this->author_name);
        $fsql = "Insert INTO book_title(booktitle, author) VALUES (?,?)";

     // echo $fsql;die();
        $result = $this->DBH->prepare($fsql);

        $result->execute($arrData);
        if($result)
            Message::setMessage("Success ! Data has been inserted Successfully :)");
        else
            Message::setMessage("Failed ! Data has not been inserted Successfully ):");
        Utility::redirect('create.php');



    }
}